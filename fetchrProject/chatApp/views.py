from django.shortcuts import render
from .models import Chat
from django.http import JsonResponse , HttpResponse
# Create your views here.

def welcome(request):
    if request.method == "POST":
        print "111"
        if request.is_ajax():     
            print "222"  
            data = request.POST.get('message')
            usr  = request.POST.get('usr')
            u = Chat(author = usr ,message = data)
            u.save()
            
            data = {"user":usr ,"message" : data}
            return JsonResponse(data)
    else:
        msgs = Chat.objects.all()
        return render(request,'chatApp/home.html', {'msgs' : msgs})
            
def messages(request):
    c = Chat.objects.all()
    return render(request, 'chatApp/messages.html', {'msgs': c})
